package com.media.kurakaninepal;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SplashScreenActivity extends AppCompatActivity {

    ImageView image;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        image1();


    }

    public void image1(){

        image=(ImageView)findViewById(R.id.imgkendra);
        Animation myanim = AnimationUtils.loadAnimation(this,R.anim.mytransition );
        image.startAnimation(myanim);
        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        progressBar.setVisibility(View.VISIBLE);



        if (isconnected()) {


            Thread timer = new Thread() {

                @Override
                public void run() {

                    try {
                        sleep(3000);
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                        super.run();


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }

//                else {
//
//                    Toast.makeText(SplashScreen.this, "No Internet Connection!!! Please Try Again Later...", Toast.LENGTH_SHORT).show();
//
//
//                }

            };


            timer.start();
        }

        else{
            Toast.makeText(SplashScreenActivity.this, "No Internet Connection!!! Please Try Again Later...", Toast.LENGTH_SHORT).show();
        }
    }



    public boolean isconnected(){
        ConnectivityManager mconn = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo minfo =mconn.getActiveNetworkInfo();
        if(minfo != null && minfo.isConnected()){
            return true;
        }
        return false;
    }




}







